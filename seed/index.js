/* eslint-disable object-curly-newline */
require('dotenv').config();
const readline = require('readline-sync');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const User = require('../src/models/User');
const Category = require('../src/models/Category');

const users = require('./users');
const categories = require('./category');

const answer = readline.question(
  'Are you sure to seed DB? \nIt removes ALL COLLECTIONS completly first!\n(yes\\No):',
);

if (answer !== 'yes') {
  console.log('Stop seeding.');
  process.exit();
}

const connect = async () => {
  try {
    await mongoose.connect(process.env.DB_URL, {
      useCreateIndex: true,
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    console.log('[mongodb] connected');

    await mongoose.connection.db.dropDatabase();
    console.log('[mongodb] DB is dropped');

    await Promise.all(
      users.map(async ({ email, password, name, acronym, role }) => {
        const salt = await bcrypt.genSalt();
        const hash = await bcrypt.hash(password, salt);

        return new User({ email, salt, hash, name, role, acronym }).save();
      }),
    );
    console.log('[mongodb] "users" collection is seeded');

    await Promise.all(
      categories.map((category) => new Category(category).save()),
    );
    console.log('[mongodb] "categories" collection is seeded');
  } catch (error) {
    console.log('---error', error);
  }

  await mongoose.connection.close();
  console.log('[mongodb] connection closed');
  return null;
};

connect();
