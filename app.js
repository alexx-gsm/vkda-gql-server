const moment = require('moment');
const express = require('./server');
const mount = require('./apollo');

require('moment/locale/ru');

moment.locale('ru');

const app = express();

mount(app);
