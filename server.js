require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');

mongoose
  .connect(process.env.DB_URL, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('[mongodb] connected'))
  .catch((err) => console.log('err', err));

module.exports = express;
