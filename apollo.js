require('dotenv').config();
const { ApolloServer } = require('apollo-server-express');
const schema = require('./src/graphql');

const Api = require('./src/dataSources');

const PORT = process.env.PORT || 4444;

const mount = (app) => {
  const server = new ApolloServer({
    context: async ({ req }) => {
      const token = (req.headers && req.headers.authorization) || '';
      const user = await Api.AuthApi.checkUser(token);
      return { user };
    },
    schema,
    dataSources: () => Api,
  });

  server.applyMiddleware({ app, path: '/graphql' });

  app.listen(PORT, () => console.log(`[app]: ${PORT}`));
};

module.exports = mount;
