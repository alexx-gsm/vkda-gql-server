/* eslint-disable global-require */
module.exports = {
  AuthApi: require('./authApi'),
  UserApi: require('./userApi'),
  CategoryApi: require('./categoryApi'),
  DishApi: require('./dishApi'),
  MenuApi: require('./menuApi'),
  CourierApi: require('./courierApi'),
  CustomerApi: require('./customerApi'),
  CompanyApi: require('./companyApi'),
};
