const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { isEmail } = require('validator');
const User = require('../models/User');
const signUpValidate = require('../models/User/validate-signup');
const signInValidate = require('../models/User/validate-signin');
const { SERVER_ERROR } = require('../utils/errors');

module.exports = {
  /**
   * @desc   sign up
   * @access PUBLIC
   * @param  {email, password, name} = input
   * @returns user || null
   */
  async signUp(input) {
    const { isValid, errors } = signUpValidate(input);
    if (!isValid) {
      return {
        isSuccess: false,
        data: [],
        message: 'Input validation error',
        errors,
      };
    }

    const { email, password, name } = input;

    const user = await User.findOne({ email });
    if (user) {
      return {
        isSuccess: false,
        data: [],
        message: 'Email уже зарегистрирован',
        errors: [],
      };
    }

    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(password, salt);

    const newUser = await new User({
      email,
      hash,
      salt,
      name,
      role: 'user',
    }).save();

    if (newUser) {
      return {
        isSuccess: true,
        data: [newUser],
        message: 'User signup.',
        errors: [],
      };
    }
    return SERVER_ERROR;
  },

  /**
   * @desc    sign in
   * @access  PUBLIC
   * @param   {email, password} data
   * @returns token || null
   */
  async signIn(input) {
    const { isValid, errors } = signInValidate(input);
    if (!isValid) {
      return {
        isSuccess: false,
        data: [],
        message: 'Input validation error',
        errors,
      };
    }

    const { email, password } = input;
    const user = await User.findOne({ email });

    if (!user) {
      return {
        isSuccess: false,
        data: [],
        message: 'Пользователь не найден',
        errors: [],
      };
    }

    const isMatch = await bcrypt.compare(password, user.hash);

    if (!isMatch) {
      return {
        isSuccess: false,
        data: [],
        message: 'Введенные данные некорректны',
        errors: [],
      };
    }

    const token = jwt.sign({ email, role: user.role }, process.env.SECRET_KEY, {
      expiresIn: '7d',
    });

    if (token) {
      return {
        isSuccess: true,
        data: [
          {
            token: `Bearer ${token}`,
            user: {
              email: user.email,
              name: user.name,
              role: user.role,
              acronym: user.acronym,
            },
          },
        ],

        message: 'User signin.',
        errors: [],
      };
    }

    return SERVER_ERROR;
  },

  /**
   * @desc    check user by jwt-token
   * @access  SERVICE
   * @param   {email, password} data
   * @returns user || null
   */
  async checkUser(token) {
    if (!token || token.indexOf('Bearer ') !== 0) {
      return null;
    }

    try {
      const { email } = jwt.verify(
        token.split('Bearer ')[1],
        process.env.SECRET_KEY,
      );

      if (!isEmail(email)) {
        return null;
      }

      const user = await User.findOne({ email });
      return user || null;
    } catch (error) {
      return null;
    }
  },
};
