const { DataSource } = require('apollo-datasource');
const Dish = require('../models/Dish');
const createValidate = require('../models/Dish/validate-create');
const { SERVER_ERROR, UNATHORIZED_ERROR } = require('../utils/errors');

class DishAPI extends DataSource {
  initialize(config) {
    this.context = config.context;
    this.user = this.context.user;
  }

  /** GET ALL */
  async getAll() {
    const items = await Dish.find();

    return {
      isSuccess: true,
      data: items,
      message: 'Dishes',
      errors: [],
    };
  }

  /** GET ITEMS BY ID */
  async getItems(ids) {
    if (!Array.isArray(ids) || !ids.length) {
      return SERVER_ERROR;
    }

    const items = await Promise.all(ids.map((_id) => this.getOne(_id)));

    return items;
  }

  /** UPDATE */
  async update(input) {
    if (!this.isAuth) {
      return UNATHORIZED_ERROR;
    }

    const { isValid, errors } = createValidate(input);
    if (!isValid) {
      return {
        isSuccess: false,
        data: [],
        message: 'Input validation error',
        errors,
      };
    }

    const inputFields = {
      title: input.title,
      category: input.category,
      weight: input.weight,
      price: input.price,
      composition: input.composition,
      isComplex: input.isComplex,
      comment: input.comment,
    };

    const newItem = input._id
      ? await Dish.findByIdAndUpdate(input._id, inputFields, { new: true })
      : await new Dish({ ...inputFields, author: this.user }).save();

    if (newItem) {
      return {
        isSuccess: true,
        data: [newItem],
        message: 'Dish created.',
        errors: [],
      };
    }
    return SERVER_ERROR;
  }

  /** GET ONE */
  async getOne(_id) {
    return Dish.findById(_id);
  }

  get isAuth() {
    return !!this.user;
  }
}

module.exports = new DishAPI();
