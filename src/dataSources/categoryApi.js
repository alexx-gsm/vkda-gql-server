const { DataSource } = require('apollo-datasource');
const Category = require('../models/Category');
const createValidate = require('../models/Category/validate-create');
const { SERVER_ERROR, UNATHORIZED_ERROR } = require('../utils/errors');

class CategoryAPI extends DataSource {
  initialize(config) {
    this.context = config.context;
    this.user = this.context.user;
  }

  async getAll() {
    const items = await Category.find();

    return {
      isSuccess: true,
      data: items,
      message: 'Categories',
      errors: [],
    };
  }

  async getOne(_id) {
    const item = await Category.findById(_id);

    return {
      isSuccess: true,
      data: [item],
      message: 'Category',
      errors: [],
    };
  }

  async _getOne(_id) {
    return await Category.findById(_id);
  }

  async edit(input) {
    if (!this.isAuth) {
      return UNATHORIZED_ERROR;
    }

    const { isValid, errors } = createValidate(input);
    if (!isValid) {
      return {
        isSuccess: false,
        data: [],
        message: 'Input validation error',
        errors,
      };
    }

    const inputFields = {
      title: input.title,
      parent: input.parent,
      sorting: input.sorting,
      description: input.description,
    };

    const newItem = input._id
      ? await Category.findByIdAndUpdate(input._id, inputFields, { new: true })
      : await new Category({ ...inputFields, author: this.user }).save();

    if (newItem) {
      return {
        isSuccess: true,
        data: [newItem],
        message: 'Category created.',
        errors: [],
      };
    }
    return SERVER_ERROR;
  }

  get isAuth() {
    return !!this.user;
  }
}

module.exports = new CategoryAPI();
