const { DataSource } = require('apollo-datasource');
const moment = require('moment');

const Menu = require('../models/Menu');
const createValidate = require('../models/Menu/validate-create');

const { SERVER_ERROR, UNATHORIZED_ERROR } = require('../utils/errors');

class MenuAPI extends DataSource {
  initialize(config) {
    this.context = config.context;
    this.user = this.context.user;
  }

  async getAll(week = moment().isoWeek()) {
    const startDay = moment().isoWeek(week).startOf('week');
    const endDay = moment().isoWeek(week).endOf('week');

    const items = await Menu.find({
      date: {
        $gte: startDay.toDate(),
        $lte: endDay.toDate(),
      },
    }).sort({ date: 'asc' });

    return {
      isSuccess: true,
      data: items,
      message: 'Menus',
      errors: [],
    };
  }

  async getMenus() {
    const startDay = moment().startOf('day');
    const endDay = moment().add(7, 'd').endOf('day');

    const items = await Menu.find({
      date: {
        $gte: startDay.toDate(),
        $lte: endDay.toDate(),
      },
      isActive: true,
    });

    return {
      isSuccess: true,
      data: items,
      message: 'Menus',
      errors: [],
    };
  }

  async getOne(_id) {
    const item = Menu.findById(_id);

    return {
      isSuccess: true,
      data: [item],
      message: 'Menu',
      errors: [],
    };
  }

  async getOneByDate(date) {
    const startDay = moment(date).startOf('day');
    const endDay = moment(date).endOf('day');

    const item = await Menu.find({
      date: {
        $gte: startDay.toDate(),
        $lte: endDay.toDate(),
      },
      isActive: true,
    });

    return {
      isSuccess: true,
      data: item,
      message: 'Menu',
      errors: [],
    };
  }

  async update(input) {
    if (!this.isAuth) {
      return UNATHORIZED_ERROR;
    }

    const { isValid, errors } = createValidate(input);

    if (!isValid) {
      return {
        isSuccess: false,
        data: [],
        message: 'Input validation error',
        errors,
      };
    }

    const inputFields = {
      date: input.date,
      dishes: input.dishes,
      isActive: input.isActive,
    };

    const newItem = input._id
      ? await Menu.findByIdAndUpdate(input._id, inputFields, { new: true })
      : await new Menu({ ...inputFields, author: this.user }).save();

    if (newItem) {
      return {
        isSuccess: true,
        data: [newItem],
        message: 'Menu created.',
        errors: [],
      };
    }
    return SERVER_ERROR;
  }

  get isAuth() {
    return !!this.user;
  }
}

module.exports = new MenuAPI();
