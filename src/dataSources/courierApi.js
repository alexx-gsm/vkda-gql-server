const { DataSource } = require('apollo-datasource');
const Courier = require('../models/Courier');
const createValidate = require('../models/Courier/validate-create');
const { SERVER_ERROR, UNATHORIZED_ERROR } = require('../utils/errors');

class CourierAPI extends DataSource {
  initialize(config) {
    this.context = config.context;
    this.user = this.context.user;
  }

  /** GET ALL */
  async getAll() {
    const items = await Courier.find();

    return {
      isSuccess: true,
      data: items,
      message: 'Couriers',
      errors: [],
    };
  }

  /** UPDATE */
  async update(input) {
    if (!this.isAuth) {
      return UNATHORIZED_ERROR;
    }

    const { isValid, errors } = createValidate(input);
    if (!isValid) {
      return {
        isSuccess: false,
        data: [],
        message: 'Input validation error',
        errors,
      };
    }

    const inputFields = {
      title: input.title,
      comment: input.comment,
    };

    const newItem = input._id
      ? await Courier.findByIdAndUpdate(input._id, inputFields, { new: true })
      : await new Courier({ ...inputFields, author: this.user }).save();

    if (newItem) {
      return {
        isSuccess: true,
        data: [newItem],
        message: 'Courier created.',
        errors: [],
      };
    }
    return SERVER_ERROR;
  }

  async _getOne(_id) {
    if (!this.isAuth) {
      return UNATHORIZED_ERROR;
    }

    return await Courier.findById(_id);
  }

  get isAuth() {
    return !!this.user;
  }
}

module.exports = new CourierAPI();
