const { DataSource } = require('apollo-datasource');
const Company = require('../models/Company');
const createValidate = require('../models/Company/validate-create');
const { SERVER_ERROR, UNATHORIZED_ERROR } = require('../utils/errors');

class CompanyAPI extends DataSource {
  initialize(config) {
    this.context = config.context;
    this.user = this.context.user;
  }

  /** GET ALL */
  async getAll() {
    const items = await Company.find();

    if (!this.isAuth) {
      return UNATHORIZED_ERROR;
    }

    return {
      isSuccess: true,
      data: items,
      message: 'Companies',
      errors: [],
    };
  }

  /** UPDATE */
  async update(input) {
    if (!this.isAuth) {
      return UNATHORIZED_ERROR;
    }

    const { isValid, errors } = createValidate(input);
    if (!isValid) {
      return {
        isSuccess: false,
        data: [],
        message: 'Input validation error',
        errors,
      };
    }

    const inputFields = {
      title: input.title,
      address: input.address,
      comment: input.comment,
    };

    const newItem = input._id
      ? await Company.findByIdAndUpdate(input._id, inputFields, { new: true })
      : await new Company({ ...inputFields, author: this.user }).save();

    if (newItem) {
      return {
        isSuccess: true,
        data: [newItem],
        message: 'Company created.',
        errors: [],
      };
    }
    return SERVER_ERROR;
  }

  async _getOne(_id) {
    if (!this.isAuth) {
      return UNATHORIZED_ERROR;
    }

    return await Company.findById(_id);
  }

  get isAuth() {
    return !!this.user;
  }
}

module.exports = new CompanyAPI();
