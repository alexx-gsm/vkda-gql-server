const { DataSource } = require('apollo-datasource');
const { UNATHORIZED_ERROR } = require('../utils/errors');

class UserAPI extends DataSource {
  initialize(config) {
    this.context = config.context;
  }

  async test() {
    if (!this.isAuth) {
      return UNATHORIZED_ERROR;
    }
    return {
      isSuccess: true,
      data: [],
      message: 'User signup.',
      errors: [],
    };
  }

  isAuth() {
    const { user } = this.context;
    return !!user;
  }
}

module.exports = new UserAPI();
