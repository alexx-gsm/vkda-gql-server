const { DataSource } = require('apollo-datasource');
const Customer = require('../models/Customer');
const createValidate = require('../models/Customer/validate-create');
const { SERVER_ERROR, UNATHORIZED_ERROR } = require('../utils/errors');

class CustomerAPI extends DataSource {
  initialize(config) {
    this.context = config.context;
    this.user = this.context.user;
  }

  /** GET ALL */
  async getAll() {
    if (!this.isAuth) {
      return UNATHORIZED_ERROR;
    }

    const items = await Customer.find();

    return {
      isSuccess: true,
      data: items,
      message: 'Customeres',
      errors: [],
    };
  }

  /** GET ONE */
  async getOne(_id) {
    const item = Customer.findById(_id);

    return item;
  }

  /** GET ITEMS BY ID */
  async getItems(ids) {
    if (!Array.isArray(ids) || !ids.length) {
      return SERVER_ERROR;
    }

    const items = await Promise.all(ids.map((_id) => this.getOne(_id)));

    return items;
  }

  /** UPDATE */
  async update(input) {
    if (!this.isAuth) {
      return UNATHORIZED_ERROR;
    }

    const { isValid, errors } = createValidate(input);
    if (!isValid) {
      return {
        isSuccess: false,
        data: [],
        message: 'Input validation error',
        errors,
      };
    }

    const inputFields = {
      name: input.name,
      company: input.company,
      paymentType: input.paymentType,
      discount: input.discount,
      address: input.address,
      phone: input.phone,
      courier: input.courier,
      comment: input.comment,
    };

    const newItem = input._id
      ? await Customer.findByIdAndUpdate(input._id, inputFields, { new: true })
      : await new Customer({ ...inputFields, author: this.user }).save();

    if (newItem) {
      return {
        isSuccess: true,
        data: [newItem],
        message: 'Customer created.',
        errors: [],
      };
    }
    return SERVER_ERROR;
  }

  get isAuth() {
    return !!this.user;
  }
}

module.exports = new CustomerAPI();
