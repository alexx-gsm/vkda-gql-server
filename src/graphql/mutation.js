const { gql } = require('apollo-server');

module.exports = gql`
  type Mutation {
    signUp(input: SignUpInput): SignUpOutput!
    signIn(input: SignInInput): SignInOutput!

    editCategory(input: CategoryInput!): CategoryDataOutput!
    updateDish(input: DishDataInput!): DishDataOutput!
    updateMenu(input: MenuDataInput!): MenuDataOutput!

    updateCourier(input: CourierDataInput!): CourierDataOutput!
    updateCustomer(input: CustomerDataInput!): CustomerDataOutput!
    updateCompany(input: CompanyDataInput!): CompanyDataOutput!
  }
`;
