module.exports = {
  Query: {
    categories: (_root, _args, { dataSources }) => {
      const result = dataSources.CategoryApi.getAll();
      return result;
    },
    category: (_root, { id }, { dataSources }) => {
      const result = dataSources.CategoryApi.getOne(id);
      return result;
    },
  },

  Mutation: {
    editCategory: (_root, { input }, { dataSources }) => {
      const result = dataSources.CategoryApi.edit(input);
      return result;
    },
  },
};
