module.exports = {
  Query: {
    couriers: (_root, _args, { dataSources }) => {
      const result = dataSources.CourierApi.getAll();
      return result;
    },
  },

  Mutation: {
    updateCourier: (_root, { input }, { dataSources }) => {
      const result = dataSources.CourierApi.update(input);
      return result;
    },
  },
};
