const { GraphQLScalarType } = require('graphql');
const { Kind } = require('graphql/language');
const moment = require('moment');

module.exports = {
  Query: {
    menus: async (_root, { week }, { dataSources }) => {
      const result = await dataSources.MenuApi.getAll(week);
      return result;
    },
    menusFromNow: (_root, _args, { dataSources }) => {
      const result = dataSources.MenuApi.getMenus();
      return result;
    },
    menu: (_root, { id }, { dataSources }) => {
      const result = dataSources.MenuApi.getOne(id);
      return result;
    },
    menuByDate: async (_root, { date }, { dataSources }) => {
      const result = await dataSources.MenuApi.getOneByDate(date);
      return result;
    },
  },

  Menu: {
    dishes: (menu) => menu.dishes,
  },

  MenuDish: {
    category: async ({ category }, _args, { dataSources }) => {
      const result = await dataSources.CategoryApi._getOne(category._id);
      return result;
    },
  },

  Mutation: {
    updateMenu: (_root, { input }, { dataSources }) => {
      const result = dataSources.MenuApi.update(input);
      return result;
    },
  },

  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    parseValue(value) {
      return new Date(value); // value from the client
    },
    serialize(value) {
      return moment(value); // value sent to the client
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.INT) {
        return parseInt(ast.value, 10); // ast value is always in string format
      }
      return null;
    },
  }),
};
