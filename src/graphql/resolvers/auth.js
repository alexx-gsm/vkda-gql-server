module.exports = {
  Query: {
    checkToken: async (_root, { input }, { dataSources }) => {
      const result = await dataSources.AuthApi.checkUser(input);
      return result;
    },
  },

  Mutation: {
    signUp: async (_root, { input }, { dataSources }) => {
      const result = await dataSources.AuthApi.signUp(input);
      return result;
    },

    signIn: async (_root, { input }, { dataSources }) => {
      const result = await dataSources.AuthApi.signIn(input);
      return result;
    },
  },
};
