module.exports = {
  Query: {
    companies: (_root, _args, { dataSources }) => {
      return dataSources.CompanyApi.getAll();
    },
  },

  Mutation: {
    updateCompany: (_root, { input }, { dataSources }) => {
      return dataSources.CompanyApi.update(input);
    },
  },
};
