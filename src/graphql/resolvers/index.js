const merge = require('lodash.merge');

const AuthResolvers = require('./auth');
const UserResolvers = require('./user');
const CategoryResolvers = require('./category');
const DishResolvers = require('./dish');
const MenuResolvers = require('./menu');
const CourierResolvers = require('./courier');
const CustomerResolvers = require('./customer');
const CompanyResolvers = require('./company');

module.exports = merge(
  AuthResolvers,
  UserResolvers,
  CategoryResolvers,
  DishResolvers,
  MenuResolvers,
  CourierResolvers,
  CustomerResolvers,
  CompanyResolvers,
);
