module.exports = {
  Query: {
    customers: (_root, _args, { dataSources }) => {
      return dataSources.CustomerApi.getAll();
    },
  },

  Customer: {
    courier: (customer, _args, { dataSources }) => {
      return dataSources.CourierApi._getOne(customer.courier);
    },
    company: (customer, _args, { dataSources }) => {
      if (!customer.company || customer.company === '-') {
        return {};
      }
      return dataSources.CompanyApi._getOne(customer.company);
    },
  },

  Mutation: {
    updateCustomer: (_root, { input }, { dataSources }) => {
      return dataSources.CustomerApi.update(input);
    },
  },
};
