module.exports = {
  Query: {
    test: async (_root, _args, { dataSources }) => {
      const result = await dataSources.UserApi.test();
      return result;
    },
  },
};
