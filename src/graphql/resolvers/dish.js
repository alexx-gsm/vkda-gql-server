const Category = require('../../models/Category');

module.exports = {
  Query: {
    dishes: (_root, _args, { dataSources }) => {
      const result = dataSources.DishApi.getAll();
      return result;
    },
    dish: (_root, { id }, { dataSources }) => {
      const result = dataSources.DishApi.getOne(id);
      return result;
    },
  },

  Dish: {
    category: async (dish) => {
      const result = await Category.findById(dish.category);
      return result;
    },
  },

  Mutation: {
    updateDish: (_root, { input }, { dataSources }) => {
      const result = dataSources.DishApi.update(input);
      return result;
    },
  },
};
