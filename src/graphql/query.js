const { gql } = require('apollo-server');

module.exports = gql`
  type Query {
    test: TestOutput!
    checkToken(input: String!): User

    categories: CategoryDataOutput!
    category(id: ID!): CategoryDataOutput

    dishes: DishDataOutput!
    dish(_id: ID!): DishDataOutput!

    menus(week: Int): MenuDataOutput!
    menusFromNow: MenuDataOutput!
    menu(_id: ID!): MenuDataOutput!
    menuByDate(date: Date!): MenuDataOutput!

    couriers: CourierDataOutput!
    customers: CustomerDataOutput!
    companies: CompanyDataOutput!
  }
`;
