const { makeExecutableSchema } = require('apollo-server');

const query = require('./query');
const mutation = require('./mutation');
const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');

module.exports = makeExecutableSchema({
  typeDefs: [query, mutation, ...typeDefs],
  resolvers,
});
