const { gql } = require('apollo-server');

module.exports = gql`
  type Company {
    _id: ID!
    title: String!
    address: String
    comment: String
  }

  input CompanyDataInput {
    _id: ID
    title: String!
    address: String
    comment: String
  }

  type CompanyDataOutput {
    isSuccess: Boolean!
    data: [Company]!
    message: String
    errors: [Error]!
    code: Int
  }
`;
