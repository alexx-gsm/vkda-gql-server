const AuthTypeDefs = require('./auth');
const UserTypeDefs = require('./user');
const CategoryTypeDefs = require('./category');
const DishTypeDefs = require('./dish');
const MenuTypeDefs = require('./menu');
const CourierTypeDefs = require('./courier');
const CustomerTypeDefs = require('./customer');
const CompanyTypeDefs = require('./company');
const ErrorTypeDefs = require('./error');

module.exports = [
  AuthTypeDefs,
  UserTypeDefs,
  CategoryTypeDefs,
  DishTypeDefs,
  MenuTypeDefs,
  CourierTypeDefs,
  CustomerTypeDefs,
  CompanyTypeDefs,
  ErrorTypeDefs,
];
