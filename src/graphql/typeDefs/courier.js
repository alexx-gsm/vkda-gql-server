const { gql } = require('apollo-server');

module.exports = gql`
  type Courier {
    _id: ID!
    title: String!
    comment: String
  }

  input CourierDataInput {
    _id: ID
    title: String!
    comment: String
  }

  type CourierDataOutput {
    isSuccess: Boolean!
    data: [Courier]!
    message: String
    errors: [Error]!
    code: Int
  }
`;
