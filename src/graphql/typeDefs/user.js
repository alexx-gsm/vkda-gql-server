const { gql } = require('apollo-server');

module.exports = gql`
  type User {
    _id: ID!
    email: String!
    name: String!
    role: String!
    acronym: String
  }

  type TestOutput {
    isSuccess: Boolean!
    data: [Token]!
    message: String
    errors: [Error]!
    code: Int
  }
`;
