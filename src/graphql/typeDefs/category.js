const { gql } = require('apollo-server');

module.exports = gql`
  type Category {
    _id: ID!
    title: String!
    parent: String
    sorting: String
    description: String
  }

  input CategoryInput {
    _id: ID
    title: String!
    parent: String
    sorting: String
    description: String
  }

  type CategoryDataOutput {
    isSuccess: Boolean!
    data: [Category]!
    message: String
    errors: [Error]!
    code: Int
  }
`;
