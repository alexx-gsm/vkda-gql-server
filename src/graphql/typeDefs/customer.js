const { gql } = require('apollo-server');

module.exports = gql`
  type CustomerCompany {
    _id: ID
    title: String
    address: String
  }

  type Customer {
    _id: ID!
    name: String!
    company: CustomerCompany
    paymentType: String
    discount: String
    address: String
    phone: String
    courier: Courier
    comment: String
  }

  input CustomerDataInput {
    _id: ID
    name: String!
    company: String
    paymentType: String
    discount: String
    address: String
    phone: String!
    courier: String
    comment: String
  }

  type CustomerDataOutput {
    isSuccess: Boolean!
    data: [Customer]!
    message: String!
    errors: [Error]!
    code: Int
  }
`;
