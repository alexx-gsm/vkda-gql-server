const { gql } = require('apollo-server');

module.exports = gql`
  type Dish {
    _id: ID!
    title: String!
    category: Category!
    weight: String!
    price: String!
    composition: String
    isComplex: Boolean
    comment: String
  }

  input DishDataInput {
    _id: ID
    title: String!
    category: String!
    weight: String!
    price: String!
    composition: String
    isComplex: Boolean
    comment: String
  }

  type DishDataOutput {
    isSuccess: Boolean!
    data: [Dish]!
    message: String
    errors: [Error]!
    code: Int
  }
`;
