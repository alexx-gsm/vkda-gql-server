const { gql } = require('apollo-server');

module.exports = gql`
  input SignUpInput {
    email: String!
    password: String!
    password2: String!
    name: String
  }

  input SignInInput {
    email: String!
    password: String!
  }

  type SignUpOutput {
    isSuccess: Boolean!
    data: [User]!
    message: String
    errors: [Error]!
    code: Int
  }

  type SignInOutput {
    isSuccess: Boolean!
    data: [SignInOutputData]!
    message: String
    errors: [Error]!
    code: Int
  }

  type SignInOutputData {
    token: String!
    user: User!
  }

  type Token {
    token: String!
  }
`;
