const { gql } = require('apollo-server');

module.exports = gql`
  type Error {
    label: String!
    value: String!
  }
`;
