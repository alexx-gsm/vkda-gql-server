const { gql } = require('apollo-server');

module.exports = gql`
  scalar Date

  type MenuDish {
    _id: ID
    index: Int
    title: String
    category: Category
    weight: String
    price: String
    composition: String
    isComplex: Boolean
    comment: String
  }

  type Menu {
    _id: ID
    date: Date
    dishes: [MenuDish]
    isActive: Boolean
  }

  input MenuDataInput {
    _id: ID
    date: String!
    dishes: [MenuDishDataInput]!
    isActive: Boolean
  }

  input MenuDishDataInput {
    _id: ID!
    index: Int
    title: String!
    category: MenuCategoryDataInput!
    weight: String!
    price: String!
    composition: String
    isComplex: Boolean
    comment: String
  }

  input MenuCategoryDataInput {
    _id: ID!
    title: String!
    sorting: String!
  }

  type MenuDataOutput {
    isSuccess: Boolean!
    data: [Menu]!
    message: String
    errors: [Error]!
    code: Int
  }
`;
