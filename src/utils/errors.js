exports.SERVER_ERROR = {
  isSuccess: false,
  data: [],
  message: 'Internal server error.',
  errors: [],
  code: 500,
};

exports.UNATHORIZED_ERROR = {
  isSuccess: false,
  data: [],
  message: 'Unauthorized',
  errors: [],
  code: 401,
};
