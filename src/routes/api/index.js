const express = require('express');
const authApi = require('../../dataSources/authApi');

const router = express.Router();

router.post('/token', async (req, res) => {
  const { token } = req.body;

  const user = await authApi.checkUser(token);

  if (user) {
    res.json({
      user: {
        email: user.email,
        name: user.name,
        role: user.role,
      },
    });
  } else {
    res.json({ user: {} });
  }
});

module.exports = router;
