const mongoose = require('mongoose');

const { Schema } = mongoose;

const MenuSchema = new Schema({
  date: {
    type: Date,
    required: true,
  },
  dishes: [
    {
      index: {
        type: Number,
        required: true,
      },
      title: {
        type: String,
        required: true,
      },
      category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
      },
      weight: {
        type: String,
        required: true,
      },
      price: {
        type: String,
        required: true,
      },
      composition: {
        type: String,
        default: '',
      },
      comment: {
        type: String,
        default: '',
      },
    },
  ],
  isActive: {
    type: Boolean,
    default: false,
  },
  comment: {
    type: String,
    default: '',
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
});

module.exports = mongoose.model('Menu', MenuSchema);
