const isEmpty = require('../../utils/is-empty');

module.exports = (data) => {
  const errors = [];

  const pushError = (label, value) => errors.push({ label, value });

  // dishes
  data.dishes = !isEmpty(data.dishes) ? data.dishes : [];
  if (!data.dishes.length) {
    pushError('dishes', 'Добавьте блюда в меню');
  }

  // comment
  data.comment = !isEmpty(data.comment) ? data.comment : '';

  return {
    errors,
    isValid: isEmpty(errors),
  };
};
