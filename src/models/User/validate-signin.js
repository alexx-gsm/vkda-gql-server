const Validator = require('validator');
const isEmpty = require('../../utils/is-empty');

module.exports = (data) => {
  const errors = [];

  const pushError = (label, value) => errors.push({ label, value });

  // email
  data.email = !isEmpty(data.email) ? data.email : '';
  if (Validator.isEmpty(data.email)) {
    pushError('email', 'Email обязателен');
  } else if (!Validator.isEmail(data.email)) {
    pushError('email', 'Email некорректный');
  }

  // password
  data.password = !isEmpty(data.password) ? data.password : '';
  if (Validator.isEmpty(data.password)) {
    pushError('password', 'Пароль обязателен');
  }

  return {
    errors,
    isValid: isEmpty(errors),
  };
};
