const Validator = require('validator');
const isEmpty = require('../../utils/is-empty');

module.exports = (data) => {
  const errors = [];

  const pushError = (label, value) => errors.push({ label, value });

  // name
  data.name = !isEmpty(data.name) ? data.name : '';

  // email
  data.email = !isEmpty(data.email) ? data.email : '';
  if (Validator.isEmpty(data.email)) {
    pushError('email', 'Email обязателен');
  } else if (!Validator.isEmail(data.email)) {
    pushError('email', 'Email invalid');
  }

  // password && password2
  data.password = !isEmpty(data.password) ? data.password : '';
  data.password2 = !isEmpty(data.password2) ? data.password2 : '';
  if (Validator.isEmpty(data.password)) {
    pushError('password', 'Пароль обязателен');
  } else if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    pushError('password', 'Пароль от 6 знаков');
  } else if (!Validator.equals(data.password, data.password2)) {
    pushError('password2', 'Пароли не совпадают');
  }

  return {
    errors,
    isValid: isEmpty(errors),
  };
};
