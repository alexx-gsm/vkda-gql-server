const mongoose = require('mongoose');

const { Schema } = mongoose;

const CustomerSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  company: {
    type: String,
    default: '-',
  },
  paymentType: {
    type: String,
    default: 'cash',
  },
  discount: {
    type: String,
    default: '',
  },
  address: {
    type: String,
    default: '',
  },
  phone: {
    type: String,
    required: true,
  },
  courier: {
    type: Schema.Types.ObjectId,
    ref: 'Courier',
  },
  comment: {
    type: String,
    default: '',
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
});

module.exports = mongoose.model('Customer', CustomerSchema);
