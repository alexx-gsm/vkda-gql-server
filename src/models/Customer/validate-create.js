const Validator = require('validator');
const isEmpty = require('../../utils/is-empty');

module.exports = (data) => {
  const errors = [];

  const pushError = (label, value) => errors.push({ label, value });

  // name
  data.name = !isEmpty(data.name) ? data.name : '';
  if (Validator.isEmpty(data.name)) {
    pushError('name', 'Укажите имя');
  } else if (!Validator.isLength(data.name, { min: 2, max: 50 })) {
    pushError('name', 'Имя от 2 до 50 букв');
  }

  // company
  data.company = !isEmpty(data.company) ? data.company : '-';

  // paymentType
  data.paymentType = !isEmpty(data.paymentType) ? data.paymentType : '';

  // discount
  data.discount = !isEmpty(data.discount) ? data.discount : '';

  // address
  data.address = !isEmpty(data.address) ? data.address : '';

  // phone
  data.phone = !isEmpty(data.phone) ? data.phone : '';

  // courier
  data.courier = !isEmpty(data.courier) ? data.courier : '';

  // comment
  data.comment = !isEmpty(data.comment) ? data.comment : '';

  return {
    errors,
    isValid: isEmpty(errors),
  };
};
