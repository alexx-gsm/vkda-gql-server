const mongoose = require('mongoose');

const { Schema } = mongoose;

const DishSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: 'Category',
  },
  weight: {
    type: String,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
  composition: {
    type: String,
    default: '',
  },
  isComplex: {
    type: Boolean,
    default: false,
  },
  comment: {
    type: String,
    default: '',
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
});

module.exports = mongoose.model('Dish', DishSchema);
