const Validator = require('validator');
const isEmpty = require('../../utils/is-empty');

module.exports = (data) => {
  const errors = [];

  const pushError = (label, value) => errors.push({ label, value });

  // title
  data.title = !isEmpty(data.title) ? data.title : '';
  if (Validator.isEmpty(data.title)) {
    pushError('title', 'Название обязательно');
  } else if (!Validator.isLength(data.title, { min: 2, max: 50 })) {
    pushError('title', 'Название от 2 до 50 букв');
  }

  // category
  data.category = !isEmpty(data.category) ? data.category : '';
  if (Validator.isEmpty(data.category)) {
    pushError('category', 'Категория обязательна');
  }

  // weight
  data.weight = !isEmpty(data.weight) ? data.weight : '';
  if (Validator.isEmpty(data.weight)) {
    pushError('weight', 'Вес блюда обязателен');
  }

  // price
  data.price = !isEmpty(data.price) ? data.price : '';
  if (Validator.isEmpty(data.price)) {
    pushError('price', 'Цена обязательна');
  }

  // composition
  data.composition = !isEmpty(data.composition) ? data.composition : '';

  // isComples
  data.isComples = !isEmpty(data.isComples) ? data.isComples : false;

  // comment
  data.comment = !isEmpty(data.comment) ? data.comment : '';

  return {
    errors,
    isValid: isEmpty(errors),
  };
};
