const Validator = require('validator');
const isEmpty = require('../../utils/is-empty');

module.exports = (data) => {
  const errors = [];

  const pushError = (label, value) => errors.push({ label, value });

  // title
  data.title = !isEmpty(data.title) ? data.title : '';
  if (Validator.isEmpty(data.title)) {
    pushError('title', 'Название обязательно');
  } else if (!Validator.isLength(data.title, { min: 3, max: 20 })) {
    pushError('title', 'Название от 3 до 20 букв');
  }

  // parent
  data.parent = !isEmpty(data.parent) ? data.parent : null;

  // sorting
  data.sorting = !isEmpty(data.sorting) ? data.sorting : '';

  // description
  data.description = !isEmpty(data.description) ? data.description : null;

  return {
    errors,
    isValid: isEmpty(errors),
  };
};
