const mongoose = require('mongoose');

const { Schema } = mongoose;

const CourierSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  comment: {
    type: String,
    default: '',
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
});

module.exports = mongoose.model('Courier', CourierSchema);
