const Validator = require('validator');
const isEmpty = require('../../utils/is-empty');

module.exports = (data) => {
  const errors = [];

  const pushError = (label, value) => errors.push({ label, value });

  // title
  data.title = !isEmpty(data.title) ? data.title : '';
  if (Validator.isEmpty(data.title)) {
    pushError('title', 'Название обязательно');
  } else if (!Validator.isLength(data.title, { min: 2, max: 50 })) {
    pushError('title', 'Название от 2 до 50 букв');
  }

  // address
  data.address = !isEmpty(data.address) ? data.address : '';
  if (Validator.isEmpty(data.address)) {
    pushError('address', 'Укажите адрес');
  } else if (!Validator.isLength(data.address, { min: 2, max: 100 })) {
    pushError('address', 'Название от 2 до 100 букв');
  }

  // comment
  data.comment = !isEmpty(data.comment) ? data.comment : '';

  return {
    errors,
    isValid: isEmpty(errors),
  };
};
